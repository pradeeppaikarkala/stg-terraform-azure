# Configure the Microsoft Azure Provider
provider "azurerm" {
subscription_id ="a7a291cc-f966-4769-a33b-2fa46cedc33d"
}

resource "azurerm_resource_group" "TerraformStagingTest" {
    name     = "TerraformStagingTest"
    location = "westus2"

    tags = {
        environment = "Terraform STG-Demo"
    }
}

# Create virtual network
resource "azurerm_virtual_network" "StagingTestVirtualNetwork" {
    name                = "VNT-VA-IVR-STG"
    address_space       = ["10.61.32.0/20"]
    location            = "westus2"
    resource_group_name = "${azurerm_resource_group.TerraformStagingTest.name}"

    tags = {
	 environment = "Terraform STG-Demo"
    }
}

# Create subnet
resource "azurerm_subnet" "StagingTestterraformsubnet" {
    name                 = "SUB-IVR-APP-VA-STG"
    resource_group_name  = "${azurerm_resource_group.TerraformStagingTest.name}"
    virtual_network_name = "${azurerm_virtual_network.StagingTestVirtualNetwork.name}"
    address_prefix       = "10.61.32.0/24"
}

# Create public IPs
resource "azurerm_public_ip" "stagingterraformpublicip" {
    name                         = "STG-PublicIP"
    location                     = "westus2"
    resource_group_name          = "${azurerm_resource_group.TerraformStagingTest.name}"
    allocation_method            = "Dynamic"
    domain_name_label           = "testhelloworld"

    tags = {
        environment = "Terraform STG-Demo"
		}
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "StagingTestterraformnsg" {
    name                = "NSG-SUB-IVR-APP-VA-STG"
    location            = "westus2"
    resource_group_name = "${azurerm_resource_group.TerraformStagingTest.name}"

security_rule {
    name                       = "HTTP"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform STG-Demo"
    }
}

# Create network interface
resource "azurerm_network_interface" "stagingterraformnic" {
    name                      = "STG-Terraform-NIC"
	location                  = "westus2"
    resource_group_name       = "${azurerm_resource_group.TerraformStagingTest.name}"
    network_security_group_id = "${azurerm_network_security_group.StagingTestterraformnsg.id}"

    ip_configuration {
        name                          = "STG-NicConfiguration"
        subnet_id                     = "${azurerm_subnet.StagingTestterraformsubnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.stagingterraformpublicip.id}"
    }

    tags = {
        environment = "Terraform STG-Demo"
    }
}



# Create virtual machine
resource "azurerm_virtual_machine" "STG-pai-terraformVM" {
    name                  = "STG-terraformVM"
    location              = "westus2"
    resource_group_name   = "${azurerm_resource_group.TerraformStagingTest.name}"
	network_interface_ids = ["${azurerm_network_interface.stagingterraformnic.id}"]
    vm_size               = "Standard_E2s_v3"

delete_os_disk_on_termination = "true"

  storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }
   storage_os_disk {
        name              = "STG-pai-terraformOsDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }



os_profile {
computer_name = "STG-pai-terraformvm"
admin_username = "pradeep"
admin_password = "Amma@999"
}

    os_profile_linux_config {
        disable_password_authentication = false
        /*ssh_keys {
            path     = "/home/pradeep/.ssh/authorized_keys"
            key_data = "ssh-rsa AAAAB3Nz{snip}hwhqT9h"
        } */
    }

provisioner "file" {
source      = "setup.sh"
destination = "/home/pradeep/setup.sh"
connection {
      type     = "ssh"
      user     = "pradeep"
      password = "Amma@999"
      host     = "${azurerm_public_ip.stagingterraformpublicip.fqdn}"
    }
}
  # This shell script starts our Apache server and prepares the demo environment.
  provisioner "remote-exec" {
    inline = [
     "chmod +x /home/pradeep/setup.sh",
	  "sudo /home/pradeep/setup.sh",
    ]
connection {
      type     = "ssh"
      user     = "pradeep"
      password = "Amma@999"
      host     = "${azurerm_public_ip.stagingterraformpublicip.fqdn}"
    }

}

}